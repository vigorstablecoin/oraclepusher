module.exports = [
    {
        pair: 'vigeos',
        swap_defi_vigeos: 11,
        swap_defi_vigusdt: 211,
        swap_defi_eosusdt: 12,
        //api_url: 'https://api.newdex.io/v1/price?symbol=vig111111111-vig-eos',
        api_url:'swap.defi pool having most depth 11 or 211',
        json_path: '$.data.price',
        modifier: (raw_value) =>{
            return raw_value*100000000;
        },
    },
    {
        pair: 'eosusdt',
        api_url: 'https://api.binance.com/api/v1/ticker/price?symbol=EOSUSDT',
        json_path: '$.price',
        modifier: (raw_value) =>{
            return raw_value*10000;
        },
        error_handler : (res) =>{
            if(res.code){
                return res.msg;
            }
        }
    },
    {
         pair: 'eosusd',
         api_url: 'https://api.pro.coinbase.com/products/EOS-USD/ticker',
         json_path: '$.price',
         modifier: (raw_value) =>{
             return raw_value*10000;
         },
         error_handler : (res) =>{
             if(res.message){
                 return res.message;
             }
         }
     },

    {
        pair: 'eosvigor',
        swap_defi_eosvigor: 76,
        swap_defi_vigorusdt: 1105,
        swap_defi_eosusdt: 12,
        //api_url: 'https://api.newdex.io/v1/price?symbol=eosio.token-eos-vigor',
        api_url:'swap.defi pool having most depth 76 or 1105',
        json_path: '$.data.price',
        modifier: (raw_value) =>{
            return raw_value * 10000;
        },
        error_handler : (res) =>{
            if(res[0] == "error"){
                return res.join(', ');
            }
        }
    },
    {
        pair: 'iqeos',
        api_url: 'https://api.mexc.com/api/v3/ticker/price?symbol=IQEOS',
        json_path: '$.price',
        modifier: (raw_value) =>{
            return raw_value*1000000;
        },
        error_handler : (res) =>{
            if(res.code !== 200){
                return res.msg;
            }
        }
    },
    {
        pair: 'btcusd',
        api_url: 'https://api.bitfinex.com/v1/pubticker/btcusd',
        json_path: '$.last_price',
        modifier: (raw_value) => {
            return raw_value*1e4;
        },
        error_handler : (res) =>{
            return res.message
        }
    },
    {   pair: 'ethusd',
        api_url: 'https://api.bitfinex.com/v1/pubticker/ethusd',
        json_path: '$.last_price',
        modifier: (raw_value) => {
            return raw_value*1e4;
        },
        error_handler : (res) =>{
            return res.message
        }
        }

];
