# Oraclepusher

This script fetches price data from multiple api endpoints and pushes them to vigoraclehub.

## Install the dependencies
```bash
yarn
```
or
```bash
npm install
```

rename example.config.json to config.json and edit the file.
rename example.env to .env and add your private key.

## Start the pusher
```bash
node my_pusher.js
```
or with process manager pm2
```bash
pm2 start my_pusher.js
```

The pusher accepts an array of feed objects (api endpoints), the interested data can be extracted with jsonpath. Use https://jsonpath.com/ to test the data extraction from the api response.
You can set "push_to_network" in config.json to false while testing new feeds.

## OPTIONAL Install EOS RPC Proxy ( by @ghobson and @boidcom )

RPCProxy.js provides an EOSIO RPC Proxy that works with any EOSIO chain, but is currently
configured with EOS mainnet endpoints. You control this via the file endpoints.js

Proxy features:
  * Greylist for misbehaving nodes, default 5min
  * Auto rotates among all the configured endpoints.js per call
  * Keeps metrics of HTTP response codes as well as response times per endpoint,  to monitor reliability
  * logs every call carefully
  * retries automatically specific EOS error codes: 3081001, 3010008
  * Handles properly content-type json , this avoids having to pass headers to cleos
  * Adds 20MB limit to a call for setABI code type RPC calls

To view metrics simply do:
```
curl http://127.0.0.1:3051/metrics
```
It will return the following counters per endpoint:
  - a counter per HTTP response code
  - a special counter for >5s timeouts, called '5000'
  - a counter for total calls so far
  - a counter for total elapsed time spent on calls

To install do the following steps:
edit config.json to include the local rpc proxy:
```
rpc_nodes": ["http://127.0.0.1:3051"]
```
cp example.ecosystem.config.json to ecosystem.config.json
Start both my_pusher and rpcProxy app:
```
pm2 start ecosystem.config.json
```

