
class Base_Notifier {

    constructor() {
      if (this.constructor.name == Base_Notifier) {
        throw new Error("Can't initiate an abstract class!");
      }
      // console.log(`${this.constructor.name} initialized`);

    }

    send() {
        throw new Error('You need to implement a send() function');
    }

}

module.exports = {
    Base_Notifier
};