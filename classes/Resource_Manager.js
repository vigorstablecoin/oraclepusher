
class Resource_Manager {
    constructor(Eos, accountname){
        this.eos = Eos;
        this.accountname = accountname;
        this.account = null;
        this.last_fetched = 0;
        this.cache_life = 30*1000; //keep account data for 30sec;
        

    }
    async getAccount(accountname){
        //console.log(Date.now() - this.last_fetched , this.cache_life)
        if( Date.now() - this.last_fetched < this.cache_life ){
            return false;
        }
        let res = await this.eos.api.rpc.get_account(accountname);
        if(res){
            this.account = res;
            this.last_fetched = Date.now();
        }
        return res;
    }

    async getResources(resource_names){
        await this.getAccount(this.accountname);
        let res = [];
        for(let i = 0; i < resource_names.length; i++){
            switch (resource_names[i]) {
                case "CPU":
                    res.push(this._getCPUStats())
                    break;
                case "NET":
                    res.push(this._getNETStats())
                    break;
                case "RAM":
                    res.push(this._getRAMStats())
                    break;
            
                default:
                    break;
            }
        }
        return res;
    }

    _getCPUStats() {
        
        if (this.account) {
          let res = this.account.cpu_limit;
          res.name = "CPU";
          res.perc_used = Math.round(((100 - (res.available / res.max) * 100) + Number.EPSILON) * 1000) / 1000 ;
          res.available = this._parseMicroSeconds(res.available);
          res.used = this._parseMicroSeconds(res.used);
          res.max = this._parseMicroSeconds(res.max);
          res.self_delegated = this.account.self_delegated_bandwidth !== null ? this.account.self_delegated_bandwidth.cpu_weight: 0;
          res.total_delegated = this.account.total_resources.cpu_weight;
          return Object.assign({name:''}, res);
        }
      }
      
    _getNETStats() {
        if (this.account) {

          let res = this.account.net_limit;
          res.name = "NET";
          res.perc_used =  Math.round(((100 - (res.available / res.max) * 100) + Number.EPSILON) * 1000) / 1000;
          res.available = this._parseBytes(res.available);
          res.used = this._parseBytes(res.used);
          res.max = this._parseBytes(res.max);
          res.self_delegated = this.account.self_delegated_bandwidth !== null ? this.account.self_delegated_bandwidth.net_weight: 0;
          res.total_delegated = this.account.total_resources.net_weight;
          return Object.assign({name:''}, res);
        }
      }
    
    _getRAMStats() {
        if (this.account) {
          let quota = this.account.ram_quota;
          let usage = this.account.ram_usage;
          let res = {};
          res.name = "RAM";
          res.perc_used = Math.round(( (usage / quota * 100) + Number.EPSILON) * 1000) / 1000 ;
          res.available_bytes = quota-usage;
          res.available = this._parseBytes(quota-usage);
          res.used = this._parseBytes(usage);
          res.max = this._parseBytes(quota);
          return Object.assign({name:''}, res);
        }
      }

    _parseMicroSeconds(us, precision = 2) {
        us = Number(us);
        if (us >= 1000000) {
          let sec = (us / 1000000).toFixed(precision);
          return sec + " sec";
        } else if (us >= 1000) {
          let ms = (us / 1000).toFixed(precision);
          return ms + " ms";
        } else {
          let u = us.toFixed(precision);
          return u + " µs";
        }
    }
      
    _parseBytes(b, precision = 2) {
          b = Number(b);
          return `${(b/1024).toFixed(precision)} KB`;
      
    }

}

module.exports = {
    Resource_Manager
};