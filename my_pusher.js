const { Pusher } = require("./classes/Pusher");
const { Telegram_Notifier } = require("./classes/Telegram_Notifier");
const CONF = require('./config.json');
const plugins = [];

let feeds = require('./feeds');

/*
Uncomment the next line to enable telegram notifications via @VigorDACbot 
    - add your telegram handle in config.json (see example.config.json)
    - ask for the telegram bot secret in the custodian channel and add it to your .env file (see example.env)
    - start a conversation with @VigorDACbot on telegram
*/
//plugins.push(new Telegram_Notifier(CONF.telegram_handle) );

const my_pusher = new Pusher(feeds, plugins);
